package org.springframework.samples.petclinic.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.*;
import org.junit.runner.RunWith;

import javax.xml.crypto.Data;
import java.util.*;

@RunWith(Theories.class)
public class TheoryTest {

    private Owner owner_ubder_test;
    private Set<Pet> input;
    private List<Pet> expectedResult;

    @Before
    public void setUp()  {
        owner_ubder_test = new Owner();
    }

    @After
    public void tearDown() {
    }

    @DataPoints("ignoreNew values")
    public static boolean [] ignoreName (){
        return new boolean[]{true,false};
    }

    @DataPoints("name values")
    public static String [] names(){
        return new String[]{"Leo","Basil"};
    }

    @DataPoints("id values")
    public static int []id(){
        return new int[]{1,2};
    }

    @DataPoints("date values")
    public static Date[]date(){
        Date data1 = new Date(2000, Calendar.OCTOBER,7);
        Date data2 = new Date(2002, Calendar.SEPTEMBER,6);
        return new Date[]{data1,data2};
    }
    @Theory
//    @DataPoints("pet values")
    public void  pet(String name , int id , Date date, boolean iN) {
        Set<Pet> petset = new HashSet<>();
        Pet p = new Pet();
        p.setBirthDate(date);
        p.setId(id);
        p.setName(name);
        owner_ubder_test.addPet(p);
        Assert.assertEquals(owner_ubder_test.getPet(name,iN),p);

    }
    @Theory
    public void  pet2(String name , int id , Date date) {
        Set<Pet> petset = new HashSet<>();
        Pet p = new Pet();
        p.setBirthDate(date);
        p.setId(id);
        p.setName(name);
        owner_ubder_test.addPet(p);
        Assert.assertEquals(owner_ubder_test.getPet(name),p);

    }
    @Theory
    public void getPet1(String name , boolean iN) {
        Assert.assertEquals(null,owner_ubder_test.getPet(name,iN));
    }
}
