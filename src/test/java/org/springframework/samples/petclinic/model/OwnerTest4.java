package org.springframework.samples.petclinic.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OwnerTest4 {
    private Owner owner_ubder_test;

    @Before
    public void setUp()  {
        owner_ubder_test = new Owner();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAddress() {
        final Field field;
        try {
            field = owner_ubder_test.getClass().getDeclaredField("address");
            field.setAccessible(true);
            field.set(owner_ubder_test, "110 W. Liberty St.");
            assertEquals(owner_ubder_test.getAddress(), "110 W. Liberty St.");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void setAddress() throws NoSuchFieldException, IllegalAccessException {
        owner_ubder_test.setAddress("110 W. Liberty St.");
        final Field field = owner_ubder_test.getClass().getDeclaredField("address");
        field.setAccessible(true);
        assertEquals(field.get(owner_ubder_test), "110 W. Liberty St.");
    }

    @Test
    public void getCity() {
        final Field field;
        try {
            field = owner_ubder_test.getClass().getDeclaredField("city");
            field.setAccessible(true);
            field.set(owner_ubder_test, "Sun Prairie");
            assertEquals(owner_ubder_test.getCity(), "Sun Prairie");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void setCity() throws IllegalAccessException, NoSuchFieldException {
        owner_ubder_test.setCity("Sun Prairie");
        final Field field = owner_ubder_test.getClass().getDeclaredField("city");
        field.setAccessible(true);
        assertEquals(field.get(owner_ubder_test), "Sun Prairie");
    }

    @Test
    public void getTelephone() {
        final Field field;
        try {
            field = owner_ubder_test.getClass().getDeclaredField("telephone");
            field.setAccessible(true);
            field.set(owner_ubder_test, "6085551749");
            assertEquals(owner_ubder_test.getTelephone(), "6085551749");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();

        }
    }

    @Test
    public void setTelephone() throws NoSuchFieldException, IllegalAccessException {
        owner_ubder_test.setTelephone("6085551749");
        final Field field = owner_ubder_test.getClass().getDeclaredField("telephone");
        field.setAccessible(true);
        assertEquals(field.get(owner_ubder_test), "6085551749");
    }

    @Test
    public void getPetsInternal() {
//        final Field field;
//        try {
//            field = owner_ubder_test.getClass().getDeclaredField("city");
//            field.setAccessible(true);
//            field.set(owner_ubder_test, "Sun Prairie");
//            assertEquals(owner_ubder_test.getAddress(), "1Sun Prairie");
//        } catch (NoSuchFieldException | IllegalAccessException e) {
//            e.printStackTrace();
//
//        }
    }

    @Test
    public void setPetsInternal() {

//        owner_ubder_test.setPetsInternal();

    }


    @Test
    public void addPet() {
    }

}
