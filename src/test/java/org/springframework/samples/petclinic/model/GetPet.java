package org.springframework.samples.petclinic.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class GetPet {
    private Owner owner_ubder_test;
    private Set<Pet> input;
    private List<Pet> expectedResult;

    @Before
    public void setUp()  {
        owner_ubder_test = new Owner();
    }

    @After
    public void tearDown()  {
    }

    public GetPet(Set<Pet> input, ArrayList<Pet> expectedResult) {
        this.input = input;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
//    1, 'Leo', '2000-09-07', 1, 1)
    public static Collection Pets() {
        List<Set<Pet>> testParme = new ArrayList<>();
        testParme.add(new HashSet<>());
        Pet p = new Pet();
        p.setBirthDate(new Date(2000, Calendar.OCTOBER, 7));
        p.setId(1);
        p.setName("Leo");
        Set<Pet> petset = new HashSet<>();
        petset.add(p);
        testParme.add(petset);
        return Arrays.asList(new Object[][]{
            {null, new ArrayList<>(testParme.get(0))},
            {petset, new ArrayList<>(testParme.get(1))}
        });
    }

    @Test
    public void getPets() throws NoSuchFieldException, IllegalAccessException {
        final Field field = owner_ubder_test.getClass().getDeclaredField("pets");
        field.setAccessible(true);
        field.set(owner_ubder_test, this.input);
        assertEquals(owner_ubder_test.getPets(), this.expectedResult);
    }
}
